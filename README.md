

Plantilla para describir los problemas, Proporcionada por autonomic.mid
d### What happens

The description of what happens.

### What do you understand or find about that problem

Explain what do you think about what happens and/or what do you find in your research about what happens.

### You make any workaround? What did you do?

Explain your workaround if you made one or explain why you didn’t do one.

### (Optional) Why fails your workaround?

Explain why fails your workaround.

### Evidences

Add log, screenshots, URLs, or whatever that you consider that can help to solve the problem.

### I need help with

Explain what things you need help, this helps to find a fast answer to anyone that tries to help you.
